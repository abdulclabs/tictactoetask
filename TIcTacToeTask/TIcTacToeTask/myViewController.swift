//
//  myViewController.swift
//  TIcTacToeTask
//
//  Created by Click Labs on 2/4/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit
import Foundation

class myViewController: UIViewController, UIPickerViewDelegate, UITextFieldDelegate{

    //labels...
    @IBOutlet weak var playerFirstLabel: UILabel!
    @IBOutlet weak var playerSecondLabel: UILabel!
    //choose buttons...
    @IBOutlet var pickCrossSignPicker: UIPickerView!
    @IBOutlet var pickNoughtSignPicker: UIPickerView!
    //textfields to enter player name...
    @IBOutlet weak var firstPlayerTextField: UITextField!
    @IBOutlet weak var secondPlayerTextField: UITextField!
    
    //arrays which stores all cross and nought images...
    var imageCosses = ["cross.png","cross1.png","cross2.png","cross3.png"]
    var imageNoughts = ["not.png","not1.png","not2.png","not3.png"]
   
    //delegate methods to define the number of components and rows of a picker
    func numberOfComponentsInPickerView(pickerView : UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return imageCosses.count
    }
    //set the height of the rows of picker view...
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    //set the height and with of the images in the picker view...
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var myView = UIView(frame: CGRectMake(0, 0, pickerView.bounds.width - 30, 50))
        var myImageView = UIImageView(frame: CGRectMake(150, 5, 40, 40))
        
        //choose either cross or nought...
        if pickerView.tag == 1 {
            var image = imageNoughts[row]
            myImageView.image = UIImage(named: "\(image)")
            myView.addSubview(myImageView)
        }else if pickerView.tag == 2 {
            var image = imageCosses[row]
            myImageView.image = UIImage(named: "\(image)")
            myView.addSubview(myImageView)
        }
        return myView
    }
    //select yhe picker view using tag number then select the images...
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            selectCrossIcon = imageCosses[row]//assisgn images to global variable selectCrossIcon...
        }else if pickerView.tag == 2 {
            selectNoughtsIcon = imageNoughts[row]//assisgn images to global variable selectCrossIcon...
        }
    }
    //assign players name to global variable define in ViewController Class...
    @IBAction func playButton(sender: AnyObject) {
        totalGamePlayed++
        firstPlayerNameGlobal = firstPlayerTextField.text
        secondPlayerNameGlobal = secondPlayerTextField.text
    }
   
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
         self.view.endEditing(true)
        return true
    }
    
    //when touches outside of textfield keyboard gets hide...
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    @IBAction func chooseNotButton(sender: AnyObject) {
        self.view.endEditing(true)
    }
    @IBAction func chooseCrossButton(sender: AnyObject) {
         self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //reset global variable...
        totalMatch = 0
        firstPlayerWinMatches = 0
        secondPlayerWinMatches = 0
        totalTieMatches = 0
        controllererNumber = 1
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
