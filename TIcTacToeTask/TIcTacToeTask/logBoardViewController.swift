//
//  logBoardViewController.swift
//  TIcTacToeTask
//
//  Created by Click Labs on 2/10/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit
var play:[String] = [""]

class logBoardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var historyTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) ->   Int{
            return play.count
           }
   //returns the number of cells which stores the data...
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        cell.textLabel?.sizeToFit()
        cell.textLabel?.numberOfLines = 6
        cell.textLabel?.text = play[indexPath.row]
        return cell
    }
    
    override func viewWillAppear(animated: Bool) {
        //let an empty array which store all items as object...
        play = []
        
        //convert all itmes into object and store in a variable...
        if var storedRecord: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("play"){
                for var i = 0; i < storedRecord.count; ++i
                {
                    play.append(storedRecord[i] as NSString)
                }
        }
        historyTableView.reloadData()
    }
    
    //function which is used to delete the cell on sliding left...
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
            play.removeAtIndex(indexPath.row)
            historyTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            
            //this code is used to delete the cell permanently...
            let fixedtoDoItem = play
            NSUserDefaults.standardUserDefaults().setObject(fixedtoDoItem, forKey: "play")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
}
