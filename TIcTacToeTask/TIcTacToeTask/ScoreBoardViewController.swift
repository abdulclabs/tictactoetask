//
//  ScoreBoardViewController.swift
//  TIcTacToeTask
//
//  Created by Click Labs on 2/4/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ScoreBoardViewController: UIViewController {
    
    //container which contains all labels view...
    @IBOutlet weak var scoreContainer: UIView!
    //labels...
    @IBOutlet weak var playerOneNameDisplaylabel: UILabel!
    @IBOutlet weak var playerTwoNameDisplaylabel: UILabel!
    @IBOutlet weak var playerOneWinDisplayLabel: UILabel!
    @IBOutlet weak var playerOneTieDisplayLabel: UILabel!
    @IBOutlet weak var playerOneLoseDisplayLabel: UILabel!
    @IBOutlet weak var playerTwoWinDisplayLabel: UILabel!
    @IBOutlet weak var playerTwoLoseDisplayLabel: UILabel!
    @IBOutlet weak var playerTwoTieDisplayLabel: UILabel!
    @IBOutlet weak var totalMatchesPlayed: UILabel!
    
    @IBAction func saveRecordButton(sender: AnyObject) {
        //condition to save records...
        if totalMatch != 0 && pressSaveButton == 0 {
            play.append("Game : \(totalGamePlayed)   \nPlayers :         \(firstPlayerNameGlobal)  &  \(secondPlayerNameGlobal)  \n Total Match :   \(totalMatch) \n \(firstPlayerNameGlobal) Win :      \(firstPlayerWinMatches) \n \(secondPlayerNameGlobal) Win :      \(secondPlayerWinMatches) \n Ties :               \(totalTieMatches )")
        
            //these three lines are used to store the items in table permanently...
            let fixedtoDoItem = play
            NSUserDefaults.standardUserDefaults().setObject(fixedtoDoItem, forKey: "play")
            NSUserDefaults.standardUserDefaults().synchronize()
            pressSaveButton++
        }
        //condition which prevent to store data again...
        else if pressSaveButton != 0 && controllererNumber == 2{
            //if no match played show the alert...
            let title = "Saved!!"
            let message = "Your Record Has Already Saved..."
            let okText = "Ok"
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            let okButton = UIAlertAction(title: okText, style: UIAlertActionStyle.Cancel, handler: nil)
            alert.addAction(okButton)
            presentViewController(alert, animated: true, completion: nil)
        }
        //condition which shows alert on save record when no match played...
        else if  pressSaveButton == 0{
            //if no match played show the alert...
            let title = "No Match Played!"
            let message = "Please Play The Match..."
            let okText = "Ok"
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            let okButton = UIAlertAction(title: okText, style: UIAlertActionStyle.Cancel, handler: nil)
            alert.addAction(okButton)
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if totalMatch != 0{
            //assign players name to the label...
            playerOneNameDisplaylabel.text = firstPlayerNameGlobal
            playerTwoNameDisplaylabel.text = secondPlayerNameGlobal
            scoreContainer.hidden = false
            
            //assign values Win, Loose, Tie matchs for player one...
            playerOneWinDisplayLabel.text = "\(firstPlayerWinMatches)"
            //playerOneLoseDisplayLabel.text = "\(secondPlayerWinMatches)"
            playerOneTieDisplayLabel.text = "\(totalTieMatches)"
            
            //assign values Win, Loose, Tie matchs for player two...
            playerTwoWinDisplayLabel.text = "\(secondPlayerWinMatches)"
            // playerTwoLoseDisplayLabel.text = "\(firstPlayerWinMatches)"
            playerTwoTieDisplayLabel.text = "\(totalTieMatches)"
            
            //assign total matches played to label...
            totalMatchesPlayed.text = "\(totalMatch)"
        }
        controllererNumber = 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
