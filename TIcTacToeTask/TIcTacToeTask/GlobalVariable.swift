//
//  Global.swift
//  TIcTacToeTask
//
//  Created by Click Labs on 3/10/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import Foundation
//global Variables...
var controllererNumber = 0
var pressSaveButton = 0
var firstPlayerNameGlobal = ""
var secondPlayerNameGlobal = ""
var totalMatch = 0
var firstPlayerWinMatches = 0
var secondPlayerWinMatches = 0
var totalTieMatches = 0
var totalGamePlayed = 0
var selectCrossIcon = "cross.png"
var selectNoughtsIcon = "not.png"
