//
//  ViewController.swift
//  TIcTacToeTask
//
//  Created by Click Labs on 2/3/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //variable to check how much box are pressed on playboard
    var goNumber = 1
    //set initial winner 0...
    var winner = 0
   //ceel one in playboard...
    @IBOutlet weak var button0: UIButton!
    //set the game state...
    var gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    //winning conditions...
    var winningCombinations = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6],[1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]

    //playboard buttons are pressed here...
    @IBAction func buttonPressed(sender: AnyObject) {
        
        //check the game states and select the cros and nought images...
        if (gameState[sender.tag-1]==0 && winner == 0)
        {
            var image = UIImage()
            if(goNumber%2 == 0){
                image = UIImage(named: "\(selectCrossIcon)")!
                gameState[sender.tag-1] = 2
            }else{
                 image = UIImage(named: "\(selectNoughtsIcon)")!
                 gameState[sender.tag-1] = 1
            }
            //compare all winning conditions...
            for combination in winningCombinations{
                if(gameState[combination[0]]==gameState[combination[1]] && gameState[combination[1]]==gameState[combination[2]] && gameState[combination[0]] != 0)
                    {
                        winner = gameState[combination[0]]
                    }
            }
        //check if game is played and anyone has won then...
        if(winner != 0){
                 //check if player one wins...
                if(winner == 1){
                    //show the alert message to win the match...
                    let alert = UIAlertController(title: "Tic Tac Toe", message:firstPlayerNameGlobal + " has won!", preferredStyle: UIAlertControllerStyle.Alert)
                   
                    alert.addAction(UIAlertAction(title: "Play Again", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction!) -> Void in
                                    //TODO reset the fields after press playagain action on alert...
                                    self.goNumber = 1
                                        self.winner = 0
                                    self.gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
                                    var button : UIButton
                                    for var i = 1; i < 10 ; i++ {
                                        button = self.view.viewWithTag(i) as UIButton
                                        button.setImage(nil, forState: .Normal)
                                    }
                        }))//end of alert add action...
                    
                    self.presentViewController(alert, animated: true, completion: nil)
                    //global variable...
                    firstPlayerWinMatches++ //counts how much match wins for first player...
                }
                else if winner == 2 {//check if player one wins...
                    //show the alert message to win the match...
                    let alert = UIAlertController(title: "Tic Tac Toe", message: secondPlayerNameGlobal + " has won!", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    alert.addAction(UIAlertAction(title: "Play Again", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction!) -> Void in
                                    //TODO reset the fields after press playagain action on alert...
                                    self.goNumber = 1
                                    self.winner = 0
                                    self.gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
                                    var button : UIButton
                                    for var i = 1; i < 10 ; i++ {
                                        button = self.view.viewWithTag(i) as UIButton
                                        button.setImage(nil, forState: .Normal)
                                    }
                                  }))//end of alert add action...
                    
                    self.presentViewController(alert, animated: true, completion: nil)
                    //global variable...
                    secondPlayerWinMatches++ //counts how much match wins for second player...
                }
              }
        //if no one wins the show the match tie alert...
        else if winner == 0 && goNumber == 9
        {
                    totalTieMatches++ //counts how much match ties...
                    let alert = UIAlertController(title: "Tic Tac Toe", message: "It was a tie", preferredStyle: UIAlertControllerStyle.Alert)
            
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: {(alert:UIAlertAction!) -> Void in
                                    ///TODO reset the fields after press playagain action on alert...
                                    self.goNumber = 1
                                    self.winner = 0
                                    self.gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
                                    var button : UIButton
                                    for var i = 1; i < 10 ; i++ {
                                        button = self.view.viewWithTag(i) as UIButton
                                        button.setImage(nil, forState: .Normal)
                                    }
                        }))//end of alert add action...
            
                    self.presentViewController(alert, animated: true, completion: nil)
        }
            goNumber++
            sender.setImage(image, forState : .Normal)
        }
        //counts how much match played...
        totalMatch = totalTieMatches +  firstPlayerWinMatches + secondPlayerWinMatches //global variable...
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        pressSaveButton = 0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

